FROM ubuntu

LABEL maintainer="ouyangsong <songouyang@live.com>"

ADD https://github.com/songouyang/IntelliJ-IDEA-License-Server/releases/download/v1.6/IntelliJIDEALicenseServer_linux_amd64 /app/

EXPOSE 1027

RUN chmod u+x /app/IntelliJIDEALicenseServer_linux_amd64

ENTRYPOINT [ "./app/IntelliJIDEALicenseServer_linux_amd64", "-u", "ouyangsong" ]